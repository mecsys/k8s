Este procedimento é realizado em 02 passos:

1. Criar uma "application" no gitlab

* gitlab / user settings / applications / criar aplicacao


2. No Rancher, va ao seu projeto / workload / pipeline

* Rancher / projeto / workloads / pipelines / configure repositorio

	* config now / selecionar GitLab e informar "application id e secret"
          selectionar "Use a private gitlab enterprise installation"
          e informar o servidor gitlab.ic.unicamp.br
        * autorizar a "application"
        * procurar pelo nome do repo e habilitar
        * finalizar com "done"

        * agora eh rodar pela primeira vez na mao, ou iniciar automaticamento com um commit
